#!/bin/env python3
import os
import glob
import tarfile
import requests

midori = "~/Loc-OS/lpkg64bit/midori-9.0-1.lpkg"
qemu = "~/Loc-OS/lpkg64bit/qemu-7.1.0-1.lpkg"

def sis(lpkg_file):
    """Deprecated function"""
    with tarfile.open(lpkg_file, 'r') as lpkg:
        file_list = lpkg.getnames()
    print("############ FILES ##############")
    for filename in file_list:
        fn = "/"+filename.split("./")[-1]
        if fn != "/." and os.path.isfile(fn):
            print(fn)

    print("########## DIRS ##############")
    for filename in file_list:
        fn = "/"+filename.split("./")[-1]
        if fn != "/." and os.path.isdir(fn):
            print(fn)
    print("########## INODES ##################")
    inodes = [filename.split("./")[-1] for filename in file_list if filename.split("./")[-1] != "."]
    nl = lpkg_file.split("/")[-1]
    with open(f"/tmp/loc-store-{nl}.track","x") as track:
        for i in inodes:
            track.write(i+"\n")

class lpkg():
    """Library for manage lpkg files (run it as root)"""
    def __init__(self):
        self.rootdir = "/"
        self.main_dir = self.rootdir + "opt/Loc-OS-LPKG"
        self.installed = self.main_dir + "/installed-lpkg"
        self.remote_pkg = "https://gitlab.com/loc-os_linux/lpkg64bit/-/raw/main/"
        self.remote_repo = "https://gitlab.com/locosporlinux/lpkg-list/-/raw/main/"
        self.repo = self.main_dir + "/Repos/ListGitLabLocOS.LpkgRepo"
    def install(self, file: "/path/to/example-1.0-2.lpkg"):
        """Extract lpkg to root dir, create track file and recreate list"""
        with tarfile.open(file, 'r') as lpkg:
            file_list = lpkg.getnames()
            inodes = [filename.split("./")[-1] for filename in file_list]
            lpkg.extractall(path=self.rootdir)
        nl = file.split("/")[-1]
        nl = nl.split(".lpkg")[0]
        with open(f"{self.main_dir}/{nl}.track","w") as track:
            for i in inodes:
                if i != ".":
                    track.write(i+"\n")
            os.remove(self.rootdir+"desc/pkgdesc")
            os.rmdir(self.rootdir+"desc")
    def remove(self, name: str):
        """Read track file, remove files and empty folders, delete track file
        and recreate list"""
        nl = name.split("/")[-1]
        nl = nl.split(".lpkg")[0]
        inode_list = set([])
        track_files = glob.glob(f"{self.main_dir}/{nl}-*.track")
        tracks = []

        for track_file in track_files:
            if os.path.isfile(track_file):
                tracks.append(track_file)

        try:
            for track_file in tracks:
                with open(track_file) as track:
                    for inode in track:
                        inode = str(self.rootdir + inode.split("\n")[0])
                        inode_list.add(inode)
        except:
            exit(1)

        inode_list = list(inode_list)
        inode_list.sort()
        inode_list.reverse()

        for inode in inode_list:
            try:
                os.remove(inode)
                print("DEL FILE:", inode)
            except IsADirectoryError:
                if len(os.listdir(inode)) == 0:
                    print("DEL DIR:", inode)
                    os.rmdir(inode)
            except FileNotFoundError:
                print("CONTINUE:", inode)
                continue
        
        for t in track_files:
            os.remove(t)
    def is_installed(self, name):
        """Verify if track file exists and return the last version"""
        tracks = glob.glob(f"{self.installed}/{name}-*.track")
        tracks.sort()
        print(tracks)
        if len(tracks) > 0:
            last_track = tracks[-1].split("/")[-1]
            last_version = last_track.split(".track")[0].split(f"{name}-")[1]
            return last_version
        else:
            return None
    def create_list(self):
        """Call to /opt/Loc-OS-LPKG/createlistrepo 
        after download and override {self.repo}"""
        try:
            os.system("/opt/Loc-OS-LPKG/createlistrepo")
            return True
        except:
            return False
    def recreate_list(self):
        """Call to /sbin/lpkg to recreate list. Is possible do it manually?"""
        response = os.system("/sbin/lpkg recreate-list")
        if response == 0:
            return True
        else:
            return False


# print("Install")
# lpkg().install(midori)

#print("Installed?")
#print(lpkg().is_installed("midori"))

# print("Remove")
# lpkg().remove("midori")
